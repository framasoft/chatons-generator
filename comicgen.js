const d = document;
const cg = {};
const c = $('#c')[0];
// let ctx = c.getContext('2d');
let scene = new RB.Scene(c);
let w = c.width;
let h = c.height;
let fontFamily = 'Domestic_Manners, Arial, helvetica';
let pop = new Audio('pop.ogg');
let currentObj = null;

scene.add(scene.rect(w, h, 'white'));
scene.update();

const lib = $('#lib');

const miniUrls = ['accessoire1_mini.png', 'accessoire2_mini.png', 'accessoire3_mini.png', 'accessoire4_mini.png', 'corps1_mini.png', 'corps2_mini.png', 'corps3_mini.png', 'corps4_mini.png', 'moustache1_mini.png', 'moustache2_mini.png', 'moustache3_mini.png', 'moustache4_mini.png', 'nez1_mini.png', 'nez2_mini.png', 'nez3_mini.png', 'nez4_mini.png', 'yeux1_mini.png', 'yeux2_mini.png', 'yeux3_mini.png', 'yeux4_mini.png'];
const toonUrls = ['accessoire1.png', 'accessoire2.png', 'accessoire3.png', 'accessoire4.png', 'corps1.png', 'corps2.png', 'corps3.png', 'corps4.png', 'moustache1.png', 'moustache2.png', 'moustache3.png', 'moustache4.png', 'nez1.png', 'nez2.png', 'nez3.png', 'nez4.png', 'yeux1.png', 'yeux2.png', 'yeux3.png', 'yeux4.png'];

cg.clearScreen = function clearScreen() {
  // ctx = c.getContext('2d');
  scene = new RB.Scene(c);
  w = c.width;
  h = c.height;
  fontFamily = 'Domestic_Manners, Arial, helvetica';
  pop = new Audio('pop.ogg');
  currentObj = null;

  scene.add(scene.rect(w, h, 'white'));
  scene.update();
};

const keysDown = {
  shift: false,
  up: false,
  down: false,
  right: false,
  left: false,
};

$(d).keyup(e => {
  const key = e.keyCode || e.which;

  if (key === 46 && currentObj) {
    scene.remove(currentObj);
    scene.update();
    RB.destroyCanvas(currentObj.getCanvas().id);
    currentObj = null;
  }

  if (currentObj && (key === 37 || key === 39) && !keysDown.shift) {
    cg.hFlip(currentObj);
  }

  /* Reset shift + arrow keys */
  if (key === 16) {
    keysDown.shift = false;
  }
  if (key === 38) {
    keysDown.up = false;
  }
  if (key === 40) {
    keysDown.down = false;
  }
  if (key === 37) {
    keysDown.left = false;
  }
  if (key === 39) {
    keysDown.right = false;
  }
});

$(d).keydown(event => {
  const key = event.keyCode || event.which;

  if (key === 38 && currentObj && !keysDown.shift) {
    cg.zoomIn(currentObj);
  }

  if (key === 40 && currentObj && !keysDown.shift) {
    cg.zoomOut(currentObj);
  }

  /* Handle shift + arrow keys */
  if (key === 16) {
    keysDown.shift = true;
  }
  if (key === 38) {
    keysDown.up = true;
  }
  if (key === 40) {
    keysDown.down = true;
  }
  if (key === 37) {
    keysDown.left = true;
  }
  if (key === 39) {
    keysDown.right = true;
  }

  if (keysDown.shift && keysDown.up && currentObj) {
    // go up
    cg.changeZIndex(currentObj, 1);
  }
  if (keysDown.shift && keysDown.down && currentObj) {
    // go down
    cg.changeZIndex(currentObj, -1);
  }

  if (keysDown.shift && keysDown.left && currentObj) {
		// turn left
    cg.rotate(currentObj, -15);
  }
  if (keysDown.shift && keysDown.right && currentObj) {
    // turn right
    cg.rotate(currentObj, 15);
  }
});

d.onmousewheel = function onmousewheel(mw) {
  if (currentObj && mw.wheelDelta > 0) {
    cg.zoomIn(currentObj);
  } else if (currentObj && mw.wheelDelta < 0) {
    cg.zoomOut(currentObj);
  }
};

cg.buildMinis = function buildMinis() {
  let buffer = '';
  const imgString = '<img src="toons/IMG_URL" class="rc mini"></img>';
  const link = '<a href=javascript:cg.createImage("toons/IMG_URL")>';

  for (let i = 0; i < miniUrls.length; i++) {
    buffer += link.replace(/IMG_URL/, toonUrls[i]);
    buffer += imgString.replace(/IMG_URL/, miniUrls[i]);
    buffer += '</a>';
  }

  lib.append(buffer);

  // lib.append( $('#textTool').clone() );
  $('#menuContainer').append($('#instructs').clone());
};

cg.buildMinis();

cg.createImage = function createImage(url) {
  scene.image(url, obj => {
    obj.draggable = true;
    obj.setXY(w / 3, h / 4);

    obj.onmousedown = function onmousedown() {
      currentObj = obj;
      scene.zIndex(obj, 1);
      scene.update();
    };

    scene.add(obj);
    currentObj = obj;
    scene.update();
    pop.play();
  });
};

cg.createText = function createText() {
  const txt = prompt('Adicione um texto:');

  if (txt) {
    const obj = scene.text(txt, fontFamily, 26, 'black');
    obj.setXY(40, 40);
    obj.draggable = true;

    obj.onmousedown = function onmousedown() {
      currentObj = obj;
      scene.zIndex(obj, 1);
      scene.update();
    };
    currentObj = obj;

    scene.add(obj);
    scene.update();
    pop.play();
  }
};

cg.createTextFromInput = function createTextFromInput(e) {
  const key = e.keyCode || e.which;
  const txt = $('#newText').val();

  if (key === 13) {
    const obj = scene.text(txt, fontFamily, 26, 'black');
    obj.setXY(40, 40);
    obj.draggable = true;

    obj.onmousedown = function onmousedown() {
      currentObj = obj;
      scene.zIndex(obj, 1);
      scene.update();
    };
    currentObj = obj;

    scene.add(obj);
    scene.update();
    $('#newText').val('');
    pop.play();
  }
};

cg.saveImage = function saveImage() {
  const data = c.toDataURL('png');
  const win = window.open();
  const b = win.document.body;
  const img = new Image();
  img.src = data;
  b.appendChild(img);
};

cg.zoomOut = function zoomOut(obj) {
  w = obj.w * 0.05;
  h = obj.h * 0.05;

  if (obj.w - w > 0 && obj.h - h > 0) {
    obj.setDimension(obj.w - w, obj.h - h);
    obj.setCoords(obj.x + (w / 2), obj.y + (h / 2));

    scene.update();
  }
};

cg.zoomIn = function zoomIn(obj) {
  w = obj.w * 0.05;
  h = obj.h * 0.05;

  obj.setDimension(obj.w + w, obj.h + h);
  obj.setCoords(obj.x - (w / 2), obj.y - (h / 2));

  scene.update();
};

cg.changeZIndex = function changeZIndex(obj, direction) {
  if (!(getIdByObject(obj) === 1 && direction === -1)) {
    scene.zIndex(obj, direction);
    scene.update();
  }
};

cg.hFlip = function hFlip(obj) {
  const tmpCanvas = $(obj.getCanvas()).clone()[0];
  const img = obj.getCanvas();
  const tmpCtx = tmpCanvas.getContext('2d');
  w = tmpCanvas.width;
  h = tmpCanvas.height;

  // save current size and position
  const cW = obj.w;
  const cH = obj.h;
  const cX = obj.x;
  const cY = obj.y;

  tmpCtx.translate(w / 2, h / 2);
  tmpCtx.scale(-1, 1);
  tmpCtx.drawImage(img, (-1 * (w / 2)), (-1 * (h / 2)));
  tmpCanvas.id = obj.getCanvas().id;
  obj.getCanvas().id = 'killme';

  RB.destroyCanvas('killme');
  d.body.appendChild(tmpCanvas);
  obj.setCanvas(tmpCanvas);
  obj.setXY(cX, cY);
  obj.h = cH;
  obj.w = cW;
  scene.update();
};

cg.rotate = function rotate(obj, angle) {
  const img = obj.getCanvas();
  const tmpCanvas = $(img).clone()[0];
  const tmpCtx = tmpCanvas.getContext('2d');
  // tmpCtx.imageSmoothingEnabled = true;
  tmpCanvas.width += 100;
  tmpCanvas.height += 100;
  w = tmpCanvas.width;
  h = tmpCanvas.height;

  const cW = obj.w;
  const cH = obj.h;
  const cX = obj.x;
  const cY = obj.y;

  tmpCtx.translate(w / 2, h / 2);

  tmpCtx.rotate(angle * (Math.PI / 180));

  tmpCtx.drawImage(img, -cW / 2, -cH / 2);

  tmpCanvas.id = obj.getCanvas().id;
  obj.getCanvas().id = 'killme';

  RB.destroyCanvas('killme');
  d.body.appendChild(tmpCanvas);
  obj.setCanvas(tmpCanvas);
  // obj.setSCtx(tmpCtx);
  obj.setCtx(tmpCtx);
  obj.setXY(cX, cY);
  obj.h = h;
  obj.w = w;
  scene.update();
};

cg.setScreen = function setScreen(width, height) {
  if (width && height && !isNaN(width) && !isNaN(height)) {
    // var ok = confirm('All your work will be lost. Continue?');
    const ok = true;
    if (ok) {
      c.width = width;
      c.height = height;
      scene.update();
      // cg.clearScreen();
    }
  }
};
